<?php /* #?ini charset="utf-8"?

[InstanceSettings]
NomeAmministrazioneAfferente=Comune di Trento
UrlAmministrazioneAfferente=http://www.comune.trento.it

[GeneralSettings]
theme=greencycle
header_service=0
favicon=favicon.ico
favicon_src=ezimage
SocialButtons=enabled

[CreditsSettings]
Url=https://www.trentinodigitale.it/
Name=Trentino Digitale S.p.A.
Title=Trentino Digitale S.p.A.

[NetworkSettings]
PrototypeUrl=http://openpa.opencontent.it/openpa/classdefinition/

[Seo]
GoogleAnalyticsAccountID=

[Nodi]
MacroArgomenti=306
Aree=433
Argomenti=306
TipiStrutture=73

[SideMenu]
AjaxMenu=enabled
NascondiNelleClassi[]=chart

[TopMenu]
NodiAreeCustomMenu[]
NodiAreeCustomMenu[]=496
MaxRecursion=3
NodiSoloPrimoLivello[]=496

[Stili]
UsaNeiBlocchi=enabled
Nodo_NomeStile[]
Nodo_NomeStile[]=0;u-background-grey-20

[IDAttributi]
AttributoUtenteServizio=3264
AttributoUtenteUfficio=3410

[ControlloUtenti]
user_servizio_attribute_ID=3264
user_ufficio_attribute_ID=3410
user_struttura_attribute_ID=3411

[Accessibilita]
VisualizzaValidatori=disabled

[ViewSettings]
AvailableView[]=full_block





*/ ?>