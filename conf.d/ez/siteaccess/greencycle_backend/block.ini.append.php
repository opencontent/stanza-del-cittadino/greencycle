<?php /*

[PushToBlock]
ContentClasses[]
ContentClasses[]=frontpage
ContentClasses[]=homepage
ContentClasses[]=area_tematica
RootSubtree=1

[General]
AllowedTypes[]
AllowedTypes[]=Singolo
AllowedTypes[]=Lista
AllowedTypes[]=Lista3
AllowedTypes[]=Lista4
AllowedTypes[]=Eventi
AllowedTypes[]=Iosono
AllowedTypes[]=FeedRSS
AllowedTypes[]=VideoPlayer
AllowedTypes[]=ContentSearch
AllowedTypes[]=GMapItems
AllowedTypes[]=GMap
AllowedTypes[]=AreaRiservata
AllowedTypes[]=HTML

AllowedTypes[]=MappaSimple
AllowedTypes[]=MappaRelated
AllowedTypes[]=MappaReverse

[Singolo]
Name=Oggetto singolo
NumberOfValidItems=1
NumberOfArchivedItems=0
ManualAddingOfItems=enabled
ViewList[]
ViewList[]=singolo_img
ViewList[]=singolo_imgtit
ViewList[]=singolo_img_interne
ViewList[]=singolo_imgtit_interne
ViewList[]=singolo_img_interne_piccolo
ViewList[]=singolo_imgtit_interne_piccolo
ViewList[]=singolo_box_piccolo
ViewList[]=singolo_box
ViewList[]=singolo_banner
ViewName[]
ViewName[singolo_img]=Solo immagine
ViewName[singolo_imgtit]=Banner
ViewName[singolo_img_interne]=Titolo e immagine
ViewName[singolo_imgtit_interne]=Immagine e Titolo (media)
ViewName[singolo_img_interne_piccolo]=Full
ViewName[singolo_imgtit_interne_piccolo]=Immagine e Titolo (con titolo blocco)
ViewName[singolo_box_piccolo]=Line (immagine piccola con blocco titolo)
ViewName[singolo_box]=Line
ViewName[singolo_banner]=Line (con blocco titolo, sfondo grigio)


[Lista]
Name=Lista di oggetti (assegnare un contenitore)
NumberOfValidItems=1
NumberOfArchivedItems=0
CustomAttributes[]
CustomAttributes[]=node_id
UseBrowseMode[node_id]=true
CustomAttributes[]=limite
CustomAttributes[]=includi_classi
CustomAttributes[]=escludi_classi
CustomAttributes[]=ordinamento
CustomAttributes[]=livello_profondita
CustomAttributes[]=state_id
CustomAttributeNames[]
CustomAttributeNames[livello_profondita]=Livello di profondità nell'alberatura
CustomAttributeNames[limite]=Numero di elementi
CustomAttributeNames[includi_classi]=Tipologie di contenuto da includere
CustomAttributeNames[escludi_classi]=Tipologie di contenuto da escludere (alternativo rispetto al precedente)
CustomAttributeNames[ordinamento]=Ordina per
CustomAttributeNames[state_id]=Stato
CustomAttributeTypes[ordinamento]=select
CustomAttributeTypes[includi_classi]=class_select
CustomAttributeTypes[escludi_classi]=class_select
CustomAttributeSelection_ordinamento[]
CustomAttributeSelection_ordinamento[name]=Titolo
CustomAttributeSelection_ordinamento[pubblicato]=Data di pubblicazione
CustomAttributeSelection_ordinamento[modificato]=Data di ultima modifica
CustomAttributeSelection_ordinamento[priority]=Priorità del nodo
CustomAttributeTypes[state_id]=state_select
ManualAddingOfItems=disabled
ViewList[]
ViewList[]=lista_num
ViewList[]=lista_accordion
ViewList[]=lista_box
ViewList[]=lista_carousel
#ViewList[]=lista_carousel_rassegna
#ViewList[]=lista_carousel_rassegna_oggi
ViewName[]
ViewName[lista_num]=Panelli (carousel) [Mostra solo elementi con immagine]
ViewName[lista_accordion]=Panelli
ViewName[lista_box]=Elenco
ViewName[lista_carousel]=Banner (carousel) [Mostra solo elementi con immagine]
#ViewName[lista_carousel_rassegna]=Pannelli (carousel)
#ViewName[lista_carousel_rassegna_oggi]=Schede (carousel rassegna oggi)
TTL=3600

[Lista3]
Name=Lista di oggetti (assegnati singolarmente) - MAX 5
NumberOfValidItems=5
NumberOfArchivedItems=0
ManualAddingOfItems=enabled
ViewList[]
ViewList[]=lista_accordion_manual
ViewList[]=lista_box2
#ViewList[]=lista_box3
ViewList[]=lista_box4
#ViewList[]=lista_box5
ViewList[]=lista_tab
ViewList[]=lista_carousel
ViewName[]
ViewName[lista_accordion_manual]=Panelli
ViewName[lista_box2]=Box a 3 colonne (3 oggetti)
ViewName[lista_box4]=Box ultimi figli (3 oggetti)
ViewName[lista_tab]=Schede (tab)
ViewName[lista_carousel]=Banner (carousel) [Mostra solo elementi con immagine]

[Lista4]
Name=Lista di oggetti (assegnati singolarmente) - MAX 15
NumberOfValidItems=15
NumberOfArchivedItems=0
ManualAddingOfItems=enabled
ViewList[]
ViewList[]=lista_accordion_manual
ViewList[]=lista_box2
ViewList[]=lista_tab
ViewName[]
ViewName[lista_accordion_manual]=Panelli
ViewName[lista_box2]=Box a 3 colonne (3 oggetti)
ViewName[lista_tab]=Schede (tab)

[Eventi]
Name=Eventi
NumberOfValidItems=3
NumberOfArchivedItems=0
ManualAddingOfItems=enabled
CustomAttributes[]
CustomAttributes[]=tab_title
CustomAttributeNames[tab_title]=Per il secondo tab - etichetta
CustomAttributes[]=custom_interval
CustomAttributeNames[custom_interval]=Per il secondo tab - periodo di riferimento
CustomAttributeTypes[custom_interval]=select
CustomAttributeSelection_custom_interval[]
CustomAttributeSelection_custom_interval[TOMORROW]=Domani
CustomAttributeSelection_custom_interval[TOMORROW-P1W]=Da domani per una settimana
CustomAttributeSelection_custom_interval[TOMORROW-P2W]=Da domani per due settimane
CustomAttributeSelection_custom_interval[TOMORROW-P1M]=Da domani per un mese
CustomAttributeSelection_custom_interval[TOMORROW-P2M]=Da domani per due mesi
CustomAttributeSelection_custom_interval[TODAY]=Oggi
CustomAttributeSelection_custom_interval[TODAY-P1W]=Da oggi per una settimana
CustomAttributeSelection_custom_interval[TODAY-P2W]=Da oggi per due settimane
CustomAttributeSelection_custom_interval[TODAY-P1M]=Da oggi per un mese
CustomAttributeSelection_custom_interval[TODAY-P2M]=Da oggi per due mesi
CustomAttributes[]=custom_filter
CustomAttributeNames[custom_filter]=Per il secondo tab - filtra solo eventi
CustomAttributeTypes[custom_filter]=select
CustomAttributeSelection_custom_filter[]
CustomAttributeSelection_custom_filter[NULL]=Tutti gli eventi (nessun filtro)
CustomAttributeSelection_custom_filter[SPECIAL]=Eventi speciali
CustomAttributeSelection_custom_filter[MANIFESTAZIONE]=Eventi di tipo manifestazione
CustomAttributeSelection_custom_filter[MANIFESTAZIONE+SPECIAL]=Eventi speciali di tipo manifestazione
ViewList[]
ViewList[]=eventi
ViewList[]=eventi_manual
ViewList[]=eventi_carousel
ViewName[eventi]=Eventi
ViewName[eventi_manual]=Eventi singoli
ViewName[eventi_carousel]=Panelli (carousel)

[Iosono]
Name=Schede Homepage (Io sono, eventi della vita, ecc...)
NumberOfValidItems=5
NumberOfArchivedItems=0
ManualAddingOfItems=enabled
ViewList[]
ViewList[]=iosono
ViewName[]
ViewName[iosono]=Schede (tab)

[FeedRSS]
Name=Feed reader
ManualAddingOfItems=disabled
CustomAttributes[]
CustomAttributes[]=source
CustomAttributes[]=limit
CustomAttributes[]=offset
ViewList[]
ViewList[]=feed_reader
ViewList[]=feed_meteo
ViewName[]
ViewName[feed_reader]=Feed reader
ViewName[feed_meteo]=Feed meteo

[VideoPlayer]
Name=Video Player
NumberOfValidItems=3
NumberOfArchivedItems=0
ManualAddingOfItems=enabled
ViewList[]
ViewList[]=video_ez
ViewList[]=video_flow
ViewList[]=video_flow_playlist
ViewList[]=video_flow_playlist_big
ViewName[]
ViewName[video_ez]=eZ Player
ViewName[video_flow]=Flow Player
ViewName[video_flow_playlist]=Flow Player Playlist (piccola)
ViewName[video_flow_playlist_big]=Flow Player Playlist (grande)

[ContentSearch]
Name=Motori di ricerca
ManualAddingOfItems=disabled
CustomAttributes[]
CustomAttributes[]=node_id
UseBrowseMode[node_id]=true
CustomAttributes[]=class
CustomAttributes[]=attribute
ViewList[]
ViewList[]=search_class_and_attributes
ViewList[]=search_free_ajax
ViewName[]
ViewName[search_class_and_attributes]=Cerca per classe e attributi
ViewName[search_free_ajax]=Ricerca libera

[GMapItems]
Name=Google Map Items
ManualAddingOfItems=disabled
CustomAttributes[]
CustomAttributes[]=parent_node_id
CustomAttributes[]=class
CustomAttributes[]=attribute
CustomAttributes[]=limit
CustomAttributes[]=width
CustomAttributes[]=height
UseBrowseMode[parent_node_id]=true
ViewList[]
ViewList[]=geo_located_content
ViewList[]=geo_located_content_osm
ViewName[]
ViewName[geo_located_content]=Mappa (Google)
ViewName[geo_located_content_osm]=Mappa (OpenStreetMap)

[GMap]
ManualAddingOfItems=disabled
CustomAttributes[]
CustomAttributes[]=location
CustomAttributes[]=key
ViewList[]
ViewList[]=gmap
ViewName[]
ViewName[gmap]=Google Map

[AreaRiservata]
ManualAddingOfItems=disabled
CustomAttributes[]
CustomAttributes[]=parent_node_id
CustomAttributes[]=testo
CustomAttributes[]=signin
CustomAttributeTypes[testo]=text
CustomAttributeTypes[signin]=checkbox
UseBrowseMode[parent_node_id]=true
ViewList[]
ViewList[]=accesso_area_riservata
ViewName[]
ViewName[accesso_area_riservata]=Accesso area riservata

[HTML]
Name=Codice HTML
ManualAddingOfItems=disabled
CustomAttributes[]
CustomAttributes[]=html
CustomAttributeTypes[html]=text
ViewList[]
ViewList[]=html
ViewName[html]=html



*/ ?>